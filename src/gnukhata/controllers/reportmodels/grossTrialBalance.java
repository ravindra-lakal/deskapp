package gnukhata.controllers.reportmodels;

public class grossTrialBalance {
	
	private String srNo;
	private String accountName;
	private String groupName;
	private String totaldr;
	private String totalcr;
	
	public grossTrialBalance(String srNo, String accountName, String groupName,
			String totaldr, String totalcr) {
		// TODO Auto-generated constructor stub
		
		super();
		this.srNo=srNo;
		this.accountName=accountName;
		this.groupName=groupName;
		this.totaldr=totaldr;
		this.totalcr=totalcr;
	}

	/**
	 * @return the srNo
	 */
	public String getSrNo() {
		return srNo;
	}

	/**
	 * @return the accountName
	 */
	public String getAccountName() {
		return accountName;
	}

	/**
	 * @return the groupName
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * @return the totaldr
	 */
	public String getTotaldr() {
		return totaldr;
	}

	/**
	 * @return the totalcr
	 */
	public String getTotalcr() {
		return totalcr;
	}

	
	@Override
	public String toString() {
		return this.accountName;
	}
	
	

}
